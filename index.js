const btn = document.createElement("button");
btn.setAttribute("data-test", "todo-add");
document.body.appendChild(btn);
const input = document.createElement("input");
input.setAttribute("data-test", "todo-input");
document.body.appendChild(input);

function addTodoElem() {
  axios
    .post("https://jsonplaceholder.typicode.com/posts", { title: "Todo" })
    .then((response) => {})
    .catch(function (err) {
      const label = document.createElement("label");
      label.setAttribute("data-test", "todo-input-label");
      label.innerHTML = "Request error";
      document.body.appendChild(label);
    });
}

btn.addEventListener("click", addTodoElem);
